FROM ubuntu:latest

#install all the tools you might want to use in your container
RUN apt-get update
RUN apt-get install curl -y
RUN apt-get install vim -y
######Time zone###
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install apache2 -y

#change working directory to root of apache webhost
WORKDIR /var/www/html

##app files
##RUN echo "<h1>Hello this test file" > /var/www/html/index.html
ADD templatemo_552_video_catalog/index.html /var/www/html/
ADD templatemo_552_video_catalog /var/www/html/

#now start the server
CMD ["apachectl", "-D", "FOREGROUND"]
